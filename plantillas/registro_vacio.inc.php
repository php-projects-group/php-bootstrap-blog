<div class="form-group">
	<label>Nombre de usuario</label>
	<input type="text" class="form-control" name="nombre" placeholder="Ejemplo: usuario_del_blog_1">
</div>
<div class="form-group">
	<label>Email</label>
	<input type="email" class="form-control" name="email" placeholder="usuario@ejemplo.com">
</div>
<div class="form-group">
	<label>Contrase&ntilde;a</label>
	<input type="password" class="form-control" name="clave1" placeholder="**********">
</div>
<div class="form-group">
	<label>Confirmar contrase&ntilde;a</label>
	<input type="password" class="form-control" name="clave2" placeholder="**********">
</div>
<br>
<button type="reset" class="btn btn-default btn-primary">Borrar campos</button>
<br>
<br>
<button type="submit" class="btn btn-default btn-primary" name="enviar">Solicitar registro</button>